# TomlConfigProvider

A dead-simple config provider module that plugs into Elixir 1.9 release
config and parses TOML config files on startup. It is meant as a
replacement for Toml.Provider, which only works with Distillery.

## Installation

Add `toml_config_provider` to your application's `mix.exs` and run `mix deps.get` as usual:

```elixir
def deps do
  [
    # ... other dependencies
    {:toml_config_provider, "~> 0.1.0"}
  ]
end
```

## Configuration

Create your runtime config files as you would with [toml-elixir](https://github.com/bitwalker/toml-elixir).
Put it somewhere outside of your build directory (think: `/etc/myapp/config.toml`):

```toml
[myapp."Myapp.Repo"]
username = "super_secret_user"
password = "super_secret_password"
database = "myapp_prod"
hostname = "localhost"
ssl = true
pool_size = 15

[myapp."MyappWeb.Endpoint"]
# Generate a secret_key_base using `mix phx.gen.secret`
secret_key_base = "foobar2000"

# As there is no way to differentate keyword lists and maps in TOML,
# key-value pairs nested two levels deep are interpreted as maps
[myapp.deeply_nested.structure]
foo = "bar"
bar = "baz"
```

Add your file as a "config provider" inside your release config in `mix.exs`:

```elixir
def project do
  [
    app: :myapp,
    version: "0.1.0",
    elixir: "~> 1.8",
    elixirc_paths: elixirc_paths(Mix.env()),
    compilers: [:phoenix, :gettext] ++ Mix.compilers(),
    start_permanent: Mix.env() == :prod,
    aliases: aliases(),
    deps: deps(),
    default_release: :prod,
    releases: releases()
  ]
end

defp releases do
  [
    prod: [
      include_executables_for: [:unix],
      config_providers: [
        {TomlConfigProvider, "/etc/myapp/config.toml"}
      ]
    ]
  ]
end
```

In your release, you should now have access to your configuration:

```elixir
iex(prod@hostname)1> Application.get_env(:myapp, Myapp.Repo)
[
  database: "myapp_prod",
  hostname: "localhost",
  password: "super_secret_password",
  pool_size: 15,
  ssl: true,
  username: "super_secret_user"
]
```
These docs can be found at [https://hexdocs.pm/toml_config_provider](https://hexdocs.pm/toml_config_provider).

