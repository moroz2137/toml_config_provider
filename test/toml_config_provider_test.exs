defmodule TomlConfigProviderTest do
  use ExUnit.Case
  doctest TomlConfigProvider

  alias TomlConfigProvider, as: Provider

  @config """
  [myapp."Myapp.Repo"]
  username = "super_secret_user"
  password = "super_secret_password"
  database = "myapp_prod"
  hostname = "localhost"
  ssl = true
  pool_size = 15

  [myapp.deeply_nested.structure]
  foo = "bar"
  baz = 15
  """
  describe "parse!/1" do
    test "parses third-level key-value pairs as lists" do
      config = Provider.parse!(@config)
      repo_config = config[:myapp][Myapp.Repo]
      assert is_list(repo_config)
      assert repo_config[:username] == "super_secret_user"
      assert repo_config[:ssl] == true
      assert repo_config[:pool_size] == 15
    end

    test "parses deeply nested key-value pairs as maps" do
      config = Provider.parse!(@config)
      deep_config = config[:myapp][:deeply_nested][:structure]
      assert is_map(deep_config)
      assert deep_config[:foo] == "bar"
      assert deep_config[:baz] == 15
    end
  end
end
