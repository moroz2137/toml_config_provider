defmodule TomlConfigProvider.MixProject do
  use Mix.Project

  def project do
    [
      app: :toml_config_provider,
      version: "0.2.0",
      elixir: "~> 1.9",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      package: package(),
      description: description(),

      # Docs
      name: "TomlConfigProvider",
      source_url: "https://gitlab.com/moroz2137/toml_config_provider",
      docs: [
        main: "readme",
        extras: ["README.md"]
      ]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: []
    ]
  end

  def description do
    """
    A dead-simple config provider module that plugs into Elixir 1.9 release
    config and parses TOML config files on startup. It is meant as a
    replacement for Toml.Provider, which only works with Distillery.
    """
  end

  def package do
    [
      maintainers: ["Karol Moroz <k.j.moroz@gmail.com>"],
      licenses: ["MIT"],
      links: %{
        "GitLab" => "https://gitlab.com/moroz2137/toml_config_provider"
      }
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:toml, "~> 0.5.2"},

      # Docs
      {:ex_doc, "~> 0.19", only: :dev, runtime: false}
    ]
  end
end
